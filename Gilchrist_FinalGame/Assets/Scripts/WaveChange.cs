﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveChange : MonoBehaviour
{
    public GameObject Wave1;
    public GameObject Wave2;
    public GameObject Wave3;
    public GameObject Wave4;
    public GameObject Wave5;
    public int currentWave;
    public Score score;
    public GameObject Win;

    void Start()
    {
        currentWave = 1;
    }

    void Update()
    {
        if (score.kills == 15)
        {
            Wave1.gameObject.SetActive(false);
            Wave2.gameObject.SetActive(true);
            currentWave = 2;
        }
        if (score.kills == 35)
        {
            Wave2.gameObject.SetActive(false);
            Wave3.gameObject.SetActive(true);
            currentWave = 3;
        }
        if (score.kills == 60)
        {
            Wave3.gameObject.SetActive(false);
            Wave4.gameObject.SetActive(true);
            currentWave = 4;
        }
        if (score.kills == 90)
        {
            Wave4.gameObject.SetActive(false);
            Wave5.gameObject.SetActive(true);
            currentWave = 5;
        }
        if (score.kills == 125)
        {
            Win.gameObject.SetActive(true);
        }
    }
}
