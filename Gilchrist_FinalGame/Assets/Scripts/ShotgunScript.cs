﻿using UnityEngine;
using System.Collections;

public class ShotgunScript : MonoBehaviour
{
    public int damage = 75;
    public float distance;
    public float range = 25f;
    public Transform Gun;
    public Camera fpscam;
    public ParticleSystem muzzleflash;
    public int magCap = 8;
    public int mag = 8;
    public int maxAmmo = 80;
    public float fireRate = 2f;
    public float reloadTime = 1f;

    private bool isReloading = false;
    private float nextTimeToFire = 0f;

    void OnEnable()
    {
        isReloading = false;
    }

    void Update()
    {
        if (isReloading)
        {
            return;
        }
        if (mag <= 0)
        {
            StartCoroutine(Reload());
            Reload();
        }

        //Shooting Key
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && maxAmmo + mag > 0)
        {
            nextTimeToFire = Time.time + (1f / fireRate);
            Shoot();
            Gun.GetComponent<Animator>().Play("Attack", -1, 0f);
            muzzleflash.Play();
        }

        //Reloading Key
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Reload());
            Reload();
        }
    }

    void Shoot()
    {
        mag -= 1;

        RaycastHit hit;
        if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            EnemyHealth enemy = hit.transform.GetComponent<EnemyHealth>();
            if (enemy != null)
            {
                if (range <= 30)
                {
                    enemy.TakeDamage(damage);
                }
                else
                {
                    enemy.TakeDamage(damage - 10);
                }
            }
        }
    }

    IEnumerator Reload()
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadTime);
        if (maxAmmo > 0)
        {
            maxAmmo = maxAmmo - (8 - mag);
            mag = magCap;
        }
        if (maxAmmo < 0)
        {
            maxAmmo = 0;
        }
        isReloading = false;
    }
}

