﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float Health = 100f;
    public Score scoreValue;

    public void TakeDamage(int damage)
    {
        Health -= damage;
        scoreValue.HitScore();
        if (Health <= 0f)
        {
            Dead();
        }
    }

    void Dead()
    {
        Destroy(gameObject);
        scoreValue.KillScore();
    }
}