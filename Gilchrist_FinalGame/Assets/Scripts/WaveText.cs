﻿using UnityEngine;
using UnityEngine.UI;

public class WaveText : MonoBehaviour
{
    public Text waveT;
    public WaveChange wc;

    // Update is called once per frame
    void Update()
    {
        waveT.text = "Wave: " + wc.currentWave;
    }
}
