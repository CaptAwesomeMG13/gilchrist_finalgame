﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GetAmmo : MonoBehaviour
{
    public M4Script m4;
    public ShotgunScript sg;

    public void RefillAmmo()
    {
        m4.maxAmmo = 300;
        sg.maxAmmo = 80;
    }
}