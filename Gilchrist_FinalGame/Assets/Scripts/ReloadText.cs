﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadText : MonoBehaviour
{
    public Text M4Text;
    public Text ShotgunText;
    public M4Script m4;
    public ShotgunScript sg;

    // Update is called once per frame
    void Update()
    {
        M4Text.text = "M4A1: " + m4.mag + "/" + m4.maxAmmo;
        ShotgunText.text = "Shotgun: " + sg.mag + "/" + sg.maxAmmo;
    }
}
