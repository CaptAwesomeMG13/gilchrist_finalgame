﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreText;
    public int playerScore = 0;
    public int killScore = 100;
    public int hitScore = 10;
    public int kills = 0;

    void Start()
    {
        scoreText.GetComponent<Text>();
    }

    void Update()
    {
        scoreText.text = "Score: " + playerScore;
    }

    public void KillScore()
    {
        playerScore += killScore;
        kills += 1;
    }
    
    public void HitScore()
    {
        playerScore += hitScore;
    }
}
