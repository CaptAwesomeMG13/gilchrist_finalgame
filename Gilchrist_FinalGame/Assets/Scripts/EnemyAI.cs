﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
	public Transform player;
	float distancefrom_player;
	public float lookRange = 60.0f;
	public float agroRange = 50.0f;
	public float moveSpeed = 4.0f;
	public float damping = 6.0f;
	public int damage = 15;
	NavMeshAgent navAgent;
	public PlayerHealth playerHealth;
	public float hitDistance = 5;
	public float hitTime = 2f;

	private bool isPreparing = false;
	private float nextTimeToAttack = 0f;
	bool m_IsPlayerInRange;

	// Use this for initialization
	void Start()
	{
		navAgent = GetComponent<NavMeshAgent>();
	}

	// Update is called once per frame
	void Update()
	{
		distancefrom_player = Vector3.Distance(player.position, transform.position);
		if (distancefrom_player < lookRange)
		{
			transform.LookAt(player);
		}

		if (distancefrom_player < agroRange)
		{
			move();
		}

		if (isPreparing)
		{
			return;
		}

		if (Time.time >= nextTimeToAttack)
		{
			nextTimeToAttack = Time.time + (1f / hitTime);

			if (distancefrom_player <= hitDistance)
			{
				Vector3 direction = player.position - transform.position + Vector3.up;
				Ray ray = new Ray(transform.position, direction);
				RaycastHit raycastHit;

				if (Physics.Raycast(ray, out raycastHit))
				{
					if (raycastHit.collider.transform == player)
					{
						Attack();
						if (playerHealth.currentHealth <= 0)
						{
							playerHealth.Died();
						}
					}
				}
			}
		}
	}

	void lookAt()
	{
		Quaternion rotation = Quaternion.LookRotation(player.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
	}

	void move()
	{
		//transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
		navAgent.SetDestination(player.position);
	}

	void Attack()
	{
		playerHealth.TakeDamage(damage);
	}
}