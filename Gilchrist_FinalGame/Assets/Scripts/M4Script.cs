﻿using UnityEngine;
using System.Collections;

public class M4Script : MonoBehaviour
{
    public int damage = 25;
    public float distance;
    public float range = 60f;
    public Transform Gun;
    public Camera fpscam;
    public ParticleSystem muzzleflash;
    public int magCap = 30;
    public int mag = 30;
    public int maxAmmo = 300;
    public float fireRate = 15f;
    public float reloadTime = 1f;

    private bool isReloading = false;
    private float nextTimeToFire = 0f;

    public GetAmmo getAmmo;

    void OnEnable()
    {
        isReloading = false;
    }

    void Update()
    {   
        if (isReloading)
        {
            return;
        }
        if (mag <= 0)
        {
            StartCoroutine(Reload());
            Reload();
        }

        //Shooting Key
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && maxAmmo + mag > 0)
        {
            nextTimeToFire = Time.time + (1f / fireRate);
            Shoot();
            Gun.GetComponent<Animator>().Play("Attack", -1, 0f);
            muzzleflash.Play();
        }

        //Reloading Key
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Reload());
            Reload();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            getAmmo.RefillAmmo();
        }
    }

    void Shoot()
    {
        mag -= 1;

        RaycastHit hit;
        if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            EnemyHealth enemy = hit.transform.GetComponent<EnemyHealth>();
            if (enemy != null)
            {
                if (range <= 30)
                {
                    enemy.TakeDamage(damage);
                }
                else
                {
                    enemy.TakeDamage(damage-10);
                }
            }
        }
    }

    IEnumerator Reload()
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadTime);
        if (maxAmmo > 0)
        {
            maxAmmo = maxAmmo - (30 - mag);
            mag = magCap;
        }
        if (maxAmmo < 0)
        {
            maxAmmo = 0;
        }
        isReloading = false;
    }
}
